<?php


namespace Module\Vendor\Provider\ContentVerify;

use Module\Vendor\Provider\BizTrait;

/**
 * Class ContentVerifyBiz
 * @package Module\Vendor\Provider\ContentVerify
 *
 * @method AbstractContentVerifyBiz[] static listAll()
 */
class ContentVerifyBiz
{
    use BizTrait;
}
